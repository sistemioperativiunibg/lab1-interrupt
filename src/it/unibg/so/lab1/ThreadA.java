package it.unibg.so.lab1;

public class ThreadA extends Thread {

	public ThreadA(String n) {
		setName(n);
	}

	@Override
	public void run() {
		System.out.println("[thread " + getName() + "]: sto andando in sleep.");
		try {
			Thread.sleep(100000);
		} catch (InterruptedException e) {
			System.out.println("[thread " + getName() + "]: eccezione ricevuta mentre ero in sleep.");
		}
	}

}
